<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Notebook extends Model
{
  use HasFactory;

  protected $fillable = ['name'];

  public function categories()
  {
    return $this->hasMany('App\Models\Category', 'notebook');
  }
}
