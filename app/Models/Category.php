<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
  use HasFactory;

  protected $fillable = ['name', 'notebook'];

  public function notebook()
  {
    return $this->belongsTo('App\Models\Notebook');
  }

  public function links()
  {
    return $this->hasMany('App\Models\Link', 'category');
  }
}
