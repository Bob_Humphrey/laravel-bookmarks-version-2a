<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Link extends Model
{
  use HasFactory;

  protected $table = 'items';

  public function category()
  {
    return $this->belongsTo('App\Models\Category');
  }
}
