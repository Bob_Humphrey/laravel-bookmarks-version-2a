<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Notebook;
use Illuminate\Http\Request;

class Notebooks extends Component
{
  public $notebooks;
  public $currentNotebook = 0;
  public $name;

  public function selectNotebook($notebookId)
  {
    $this->currentNotebook = $notebookId;
    $this->emit('notebookSelected', $notebookId);
  }

  public function mount(Request $request)
  {
    if ($request->has('notebook')) {
      $this->currentNotebook = $request->notebook;
    }
    $this->notebooks = $this->getNotebooks();
  }

  public function render()
  {
    return view('livewire.notebooks');
  }

  protected function getNotebooks()
  {
    $notebooks = Notebook::select('id', 'name')->orderBy('name')->get();
    return $notebooks;
  }
}
