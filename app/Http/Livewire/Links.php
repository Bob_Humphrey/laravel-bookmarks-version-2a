<?php

namespace App\Http\Livewire;

use App\Models\Link;
use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Links extends Component
{
  use AuthorizesRequests;

  public $links;
  public $currentCategory = 0;

  protected $listeners = ['categorySelected', 'notebookSelected'];

  public function categorySelected($category)
  {
    $this->links = $this->getLinks($category);
    $this->currentCategory = $category;
  }

  public function notebookSelected($notebookId)
  {
    $this->links = null;
    $this->currentNotebook = 0;
  }

  public function toggleReadingList($linkId)
  {
    $this->authorize('edit-reading-learning-lists');
    $link = Link::find($linkId);
    $link->reading_list = !$link->reading_list;
    $link->save();
    $this->links = $this->getLinks($this->currentCategory);
  }

  public function toggleLearningList($linkId)
  {
    $this->authorize('edit-reading-learning-lists');
    $link = Link::find($linkId);
    $link->learning_list = !$link->learning_list;
    $link->save();
    $this->links = $this->getLinks($this->currentCategory);
  }

  public function mount(Request $request)
  {
    if ($request->has('category')) {
      $this->currentCategory = $request->category;
      $this->links = $this->getLinks($request->category);
    }
  }

  public function render()
  {
    return view('livewire.links');
  }

  protected function getLinks($category)
  {
    $links = Link::select('id', 'title', 'url', 'reading_list', 'learning_list')
      ->where('category', $category)
      ->orderBy('title')
      ->get();
    return $links;
  }
}
