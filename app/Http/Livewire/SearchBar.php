<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;

class SearchBar extends Component
{
  public $query;
  public $categories;
  public $highlightIndex;

  public function mount()
  {
    $this->resetState();
  }

  public function resetState()
  {
    $this->query = '';
    $this->categories = [];
    $this->highlightIndex = 0;
  }

  // public function selectBreed()
  // {
  //   $breed = $this->breeds[$this->highlightIndex] ?? null;
  //   if ($breed) {
  //     $this->redirect(route('study', $breed['id']));
  //   }
  // }

  public function updatedQuery()
  {
    $this->categories = Category::where('name', 'like', '%' . $this->query . '%')
      ->get()
      ->toArray();
  }

  public function render()
  {
    return view('livewire.search-bar');
  }
}
