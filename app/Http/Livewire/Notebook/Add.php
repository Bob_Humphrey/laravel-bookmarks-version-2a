<?php

namespace App\Http\Livewire\Notebook;

use Livewire\Component;
use App\Models\Notebook;

class Add extends Component
{
  public $name;

  public function addNotebook()
  {
    $this->validate([
      'name' => 'required|unique:App\Models\Notebook,name'
    ]);

    $notebook = Notebook::create([
      'name' => $this->name
    ]);
    session()->flash('success', "New notebook has been added: $this->name");
    return redirect("/home?notebook=$notebook->id");
  }

  public function render()
  {
    return view('livewire.notebook.add');
  }
}
