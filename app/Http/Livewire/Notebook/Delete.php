<?php

namespace App\Http\Livewire\Notebook;

use Livewire\Component;
use App\Models\Notebook;
use Illuminate\Http\Request;

class Delete extends Component
{
  public $notebook;
  public $name;
  public $categories;
  public $currentCategory;

  public function deleteNotebook()
  {

    $this->notebook->delete();

    session()->flash('success', "Notebook has been deleted: $this->name");
    return redirect("/home");
  }

  public function mount(Request $request)
  {
    $this->notebook = Notebook::with('categories')->find($request->id);
    $this->name = $this->notebook->name;
    $this->categories = $this->notebook->categories;
    $this->currentCategory = 0;
  }

  public function render()
  {
    return view('livewire.notebook.delete');
  }
}
