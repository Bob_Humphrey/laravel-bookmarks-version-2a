<?php

namespace App\Http\Livewire\Notebook;

use Livewire\Component;
use App\Models\Notebook;
use Illuminate\Http\Request;

class Update extends Component
{
  public $notebook;
  public $name;

  public function updateNotebook()
  {
    $this->validate([
      'name' => 'required|unique:App\Models\Notebook,name'
    ]);

    $this->notebook->name = $this->name;
    $this->notebook->save();

    session()->flash('success', "Notebook has been updated: $this->name");
    return redirect("/home?notebook=$this->notebook->id");
  }

  public function mount(Request $request)
  {
    $this->notebook = Notebook::find($request->id);
    $this->name = $this->notebook->name;
  }

  public function render()
  {
    return view('livewire.notebook.update');
  }
}
