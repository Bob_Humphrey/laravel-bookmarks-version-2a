<?php

namespace App\Http\Livewire\Category;

use Livewire\Component;
use App\Models\Category;
use App\Models\Notebook;
use Illuminate\Http\Request;

class Update extends Component
{
  public $notebooks;
  public $category;
  public $notebook;
  public $name;

  public function updateCategory()
  {
    $this->validate([
      'name' => 'required',
      'notebook' => 'required|exists:App\Models\Notebook,id',
    ]);

    $this->category->notebook = $this->notebook;
    $this->category->name = $this->name;
    $this->category->save();

    session()->flash('success', "Category has been updated: $this->name");
    return redirect("/home?notebook=$this->notebook&category=$this->category->id");
  }

  public function mount(Request $request)
  {
    $this->notebooks = $this->getNotebooks();
    $this->category = Category::find($request->id);
    $this->notebook = $this->category->notebook;
    $this->name = $this->category->name;
  }

  public function render()
  {
    return view('livewire.category.update');
  }

  protected function getNotebooks()
  {
    $notebooks = Notebook::select('id', 'name')
      ->orderBy('name')
      ->get();
    return $notebooks;
  }
}
