<?php

namespace App\Http\Livewire\Category;

use Livewire\Component;
use App\Models\Category;
use App\Models\Notebook;
use Illuminate\Http\Request;

class Delete extends Component
{
  public $notebooks;
  public $category;
  public $links;
  public $notebook;
  public $name;

  public function deleteCategory()
  {
    $this->category->delete();
    session()->flash('success', "Category has been deleted: $this->name");
    return redirect("/home?notebook=$this->notebook&category=$this->category");
  }

  public function mount(Request $request)
  {
    $this->notebooks = $this->getNotebooks();
    $this->category = Category::with('links')->find($request->id);
    $this->notebook = $this->category->notebook;
    $this->name = $this->category->name;
    $this->links = $this->category->links;
  }

  public function render()
  {
    return view('livewire.category.delete');
  }

  protected function getNotebooks()
  {
    $notebooks = Notebook::select('id', 'name')
      ->orderBy('name')
      ->get();
    return $notebooks;
  }
}
