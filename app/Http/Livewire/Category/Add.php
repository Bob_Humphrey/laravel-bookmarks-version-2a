<?php

namespace App\Http\Livewire\Category;

use Livewire\Component;
use App\Models\Category;
use App\Models\Notebook;

class Add extends Component
{
  public $notebooks;
  public $notebook;
  public $name;
  public $links;

  public function addCategory()
  {
    $this->validate([
      'name' => 'required',
      'notebook' => 'required|exists:App\Models\Notebook,id',
    ]);

    $category = Category::create([
      'name' => $this->name,
      'notebook' => $this->notebook
    ]);
    session()->flash('success', "New category has been added: $this->name");
    return redirect("/home?notebook=$this->notebook&category=$category->id");
  }

  public function mount()
  {
    $this->notebooks = $this->getNotebooks();
  }

  public function render()
  {
    return view('livewire.category.add');
  }

  protected function getNotebooks()
  {
    $notebooks = Notebook::select('id', 'name')
      ->orderBy('name')
      ->get();
    return $notebooks;
  }
}
