<?php

namespace App\Http\Livewire;

use App\Models\Link;
use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class LearningList extends Component
{
  use AuthorizesRequests;

  public $links;

  public function toggleReadingList($linkId)
  {
    $this->authorize('edit-reading-learning-lists');
    $link = Link::find($linkId);
    $link->reading_list = !$link->reading_list;
    $link->save();
    $this->links = $this->getLinks();
  }

  public function toggleLearningList($linkId)
  {
    $this->authorize('edit-reading-learning-lists');
    $link = Link::find($linkId);
    $link->learning_list = !$link->learning_list;
    $link->save();
    $this->links = $this->getLinks();
  }

  public function mount()
  {
    $this->links = $this->getLinks();
  }

  public function render()
  {
    return view('livewire.learning-list');
  }

  protected function getLinks()
  {
    $links = Link::select('id', 'title', 'url', 'reading_list', 'learning_list')
      ->where('learning_list', true)
      ->orderBy('title')
      ->get();
    return $links;
  }
}
