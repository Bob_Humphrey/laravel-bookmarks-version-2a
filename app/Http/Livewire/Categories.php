<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use Illuminate\Http\Request;

class Categories extends Component
{
  public $categories;
  public $currentCategory = 0;
  public $currentNotebook = 0;

  protected $listeners = ['notebookSelected'];

  public function notebookSelected($notebookId)
  {
    $this->categories = $this->getCategories($notebookId);
    $this->currentNotebook = $notebookId;
  }

  public function selectCategory($categoryId)
  {
    $this->currentCategory = $categoryId;
    $this->emit('categorySelected', $categoryId);
  }

  public function mount(Request $request)
  {
    if ($request->has('notebook')) {
      $this->currentNotebook = $request->notebook;
    }
    if ($request->has('category')) {
      $this->currentCategory = $request->category;
    }
    if (($request->has('notebook')) && ($request->has('category'))) {
      $this->categories = $this->getCategories($request->notebook);
    }
  }

  public function render()
  {
    return view('livewire.categories');
  }

  protected function getCategories($notebookId)
  {
    $categories = Category::select('id', 'name')
      ->where('notebook', $notebookId)
      ->orderBy('name')
      ->get();
    return $categories;
  }
}
