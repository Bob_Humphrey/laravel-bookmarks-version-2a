<?php

namespace App\Http\Livewire\Link;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.link.index');
    }
}
