<?php

namespace App\Http\Livewire\Link;

use App\Models\Link;
use Livewire\Component;
use App\Models\Category;
use Illuminate\Http\Request;

class Add extends Component
{
  public $categories;
  public $category;
  public $notebook;
  public $title;
  public $url;
  public $readingList;
  public $learningList;


  public function addLink()
  {
    $this->validate([
      'category' => 'required',
      'title' => 'required',
      'url' => 'required|url',
    ]);

    $categoryItem = $this->categories->firstWhere('id', $this->category);
    $this->notebook = $categoryItem->notebook;

    $link = new Link;
    $link->category = $this->category;
    $link->title = $this->title;
    $link->url = $this->url;
    $link->reading_list = empty($this->readingList) ? false : true;
    $link->learning_list = empty($this->learningList) ? false : true;
    $link->save();
    session()->flash('success', "New note has been added: $this->title");
    return redirect("/home?notebook=$this->notebook&category=$this->category");
  }

  public function mount(Request $request)
  {
    $this->title = $request->title;
    $this->url = $request->url;
    $this->categories = $this->getCategories();
  }

  public function render()
  {
    return view('livewire.link.add');
  }

  protected function getCategories()
  {
    $categories = Category::select('id', 'name', 'notebook')
      ->orderBy('name')
      ->get();
    return $categories;
  }
}
