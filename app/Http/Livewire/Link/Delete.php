<?php

namespace App\Http\Livewire\Link;

use App\Models\Link;
use Livewire\Component;
use App\Models\Category;
use Illuminate\Http\Request;

class Delete extends Component
{
  public $link;
  public $category;
  public $title;
  public $url;
  public $readingList;
  public $learningList;
  public $categories;
  public $notebook;


  public function deleteLink()
  {
    $categoryItem = $this->categories->firstWhere('id', $this->category);
    $this->notebook = $categoryItem->notebook;

    $this->link->category = $this->category;
    $this->link->delete();
    session()->flash('success', "Note has been deleted: $this->title");
    return redirect("/home?notebook=$this->notebook&category=$this->category");
  }

  public function mount(Request $request)
  {
    $this->link = Link::find($request->id);
    $this->category = $this->link->category;
    $this->title = $this->link->title;
    $this->url = $this->link->url;
    $this->readingList = $this->link->reading_list;
    $this->learningList = $this->link->learning_list;
    $this->categories = $this->getCategories();
  }

  public function render()
  {
    return view('livewire.link.delete');
  }

  protected function getCategories()
  {
    $categories = Category::select('id', 'name', 'notebook')
      ->orderBy('name')
      ->get();
    return $categories;
  }
}
