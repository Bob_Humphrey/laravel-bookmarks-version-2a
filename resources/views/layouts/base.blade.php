<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description"
    content="A web bookmark manager created with the TALL stack - Tailwind CSS, Alpine JS, Laravel and Livewire">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->

  <!-- Styles -->
  @livewireStyles
  @bukStyles
  <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>

<body>

  {{ $slot }}

  <!-- Scripts -->
  @livewireScripts
  @bukScripts
  <script src="{{ asset('js/all.js') }}" defer></script>

</body>

</html>
