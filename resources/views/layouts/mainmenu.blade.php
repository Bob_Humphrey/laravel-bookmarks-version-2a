@php
use Illuminate\Support\Str;
$path = request()->path();
Log::info($path);
$bookmarks = Str::of($path)->exactly('/') ? true : false;
$home = Str::of($path)->contains('home') ? true : false;
$readingList = Str::of($path)->contains('reading-list') ? true : false;
$learningList = Str::of($path)->contains('learning-list') ? true : false;
$addBookmark = Str::of($path)->contains('links/create') ? true : false;
@endphp

<a href="{{ url('/') }}" class="{{ $bookmarks || $home ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Bookmarks
</a>

<a href="{{ url('/reading-list') }}" class="{{ $readingList ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Reading List
</a>
<a href="{{ url('/learning-list') }}"
  class="{{ $learningList ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Learning List
</a>
<a href="{{ url('/links/create') }}" class="{{ $addBookmark ? 'text-blue-500' : '' }} hover:text-blue-500 lg:px-2 py-2">
  Add Bookmark
</a>

@guest
  <a href="{{ route('login') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Login
  </a>
@endguest

@auth
  <a href="{{ url('/logout') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Logout
  </a>
@endauth

<div class="lg:ml-4">
  <livewire:search-bar />
</div>
