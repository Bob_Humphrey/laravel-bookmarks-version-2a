<x-layouts.base>
  <div class="font-nunito_regular">
    <nav class="bg-gray-50 py-2" x-data="{ showMenu: false }">
      <div class="flex flex-col xl:flex-row xl:justify-between xl:items-center w-11/12 mx-auto py-3">
        <div class="flex justify-between items-center">
          <div class="flex xl:justify-start"> <a href="/" class="flex flex-col xl:flex-row w-full">
              <h1 class="font-nunito_extrabold text-3xl text-blue-900 hover:text-blue-500">
                {{ config('app.name', 'Laravel Application') }} </h1>
            </a> </div>
          <div class="xl:hidden w-8 cursor-pointer" @click="showMenu=!showMenu" x-show="!showMenu">
            <x-zondicon-menu />
          </div>
          <div class="xl:hidden w-8 cursor-pointer" @click="showMenu=!showMenu" x-show="showMenu">
            <x-zondicon-close />
          </div>
        </div>
        <div
          class="hidden xl:flex flex-row justify-end text-lg font-inter_light xl:text-xl text-blue-900 leading-none pb-8 xl:pb-0">
          @include('layouts.mainmenu') </div>
        <div class="flex flex-col xl:hidden text-lg font-inter_light xl:text-xl text-blue-900 leading-none pb-8 xl:pb-0"
          x-show="showMenu"> @include('layouts.mainmenu') </div>
      </div>
    </nav>
    <main class="p-6 w-full mx-auto">
      {{-- @include('layouts.flash') --}}

      {{ $slot }}

    </main>

    @include('layouts.footer')
  </div>
</x-layouts.base>
