<div>
  <div class="grid grid-cols-12 w-full">
    <div class="col-span-3 flex self-center h-8 w-8 cursor-pointer">
      <a href={{ url('notebooks/create') }}>
        <x-heroicon-o-plus />
      </a>
    </div>
    <div class="col-span-9 font-nunito_bold text-xl text-blue-500 pl-4">
      NOTEBOOKS
    </div>
  </div>

  @include('livewire.notebook.index')

</div>
