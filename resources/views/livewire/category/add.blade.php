<div class="w-2/5 justify-center pb-6 mx-auto">

  @include('livewire.forms.flash')

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Add Category
  </h2>

  <div class="bg-gray-50 p-8 rounded border border-gray-200">

    @include('livewire.forms.category', ['disabled' => ''])

    {{-- SUBMIT BUTTON --}}

    <div class="flex justify-center">
      <button type="submit" wire:click="addCategory"
        class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
        Add
      </button>
    </div>

  </div>
