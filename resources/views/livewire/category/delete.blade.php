<div class="w-2/5 justify-center pb-6 mx-auto">

  @include('livewire.forms.flash')

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Delete Category
  </h2>

  <div class="bg-gray-50 p-8 rounded border border-gray-200">

    @include('livewire.forms.category', ['disabled' => 'disabled'])

    @if ($links->isEmpty())

      {{-- SUBMIT BUTTON --}}

      <div class="flex justify-center">
        <button type="submit" wire:click="deleteCategory"
          class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mt-6 mx-auto cursor-pointer">
          Delete
        </button>
      </div>

    @else

      {{-- ATTACHED LINKS --}}

      <div class="text-red-600 py-4">
        You cannot delete this category because it has the following
        {{ Str::of('link')->plural($links->count()) }} attached to it.
      </div>

      @include('livewire.link.index')

    @endif

  </div>
