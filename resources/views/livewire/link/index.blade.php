  <div>
    @if ($links)
      @foreach ($links as $link)
        <div class="grid grid-cols-12 {{ $loop->odd ? '' : 'bg-gray-50' }}">
          <div class="col-span-3 xl:col-span-2 flex justify-around">
            <div class="flex self-center h-4 w-4 cursor-pointer">
              <a href={{ url("/links/$link->id/edit") }}>
                <x-zondicon-edit-pencil />
              </a>
            </div>
            <div class="flex self-center h-4 w-4 cursor-pointer">
              <a href={{ url("/links/$link->id/delete") }}>
                <x-zondicon-close />
              </a>
            </div>
            <div
              class="flex self-center h-4 w-4 cursor-pointer {{ $link->reading_list ? 'text-blue-500' : 'text-gray-300' }}"
              wire:click="toggleReadingList({{ $link->id }})">
              <x-zondicon-bookmark />
            </div>
            <div
              class="flex self-center h-4 w-4 cursor-pointer {{ $link->learning_list ? 'text-blue-500' : 'text-gray-300' }}"
              wire:click="toggleLearningList({{ $link->id }})">
              <x-zondicon-education />
            </div>
          </div>

          <div class="col-span-9 xl:col-span-10 py-1 px-4 cursor-pointer ">
            <a href={{ $link->url }} target="_blank" rel="noopener noreferrer">
              {{ $link->title }}
            </a>
          </div>
        </div>
      @endforeach
    @else
      <div></div>
    @endif
  </div>
