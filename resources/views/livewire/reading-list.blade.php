<div class="w-3/4 mx-auto">
  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Reading List
  </h2>

  @include('livewire.link.index')

</div>
