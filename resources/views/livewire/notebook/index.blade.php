<div>
  @if ($notebooks)
    @foreach ($notebooks as $notebook)
      @php
      $bgColor = $loop->odd ? '' : 'bg-gray-50';
      $textColor = 'text-black';
      if ($notebook->id == $currentNotebook) {
      $bgColor = 'bg-blue-500';
      $textColor = 'text-white';
      }
      @endphp
      <div class="grid grid-cols-12 {{ $bgColor }} {{ $textColor }}">
        <div class="col-span-3 flex justify-around">
          <div class="flex self-center h-4 w-4 cursor-pointer">
            <a href={{ url("/notebooks/$notebook->id/edit") }}>
              <x-zondicon-edit-pencil />
            </a>
          </div>
          <div class="flex self-center h-4 w-4 cursor-pointer">
            <a href={{ url("/notebooks/$notebook->id/delete") }}>
              <x-zondicon-close />
            </a>
          </div>
        </div>
        <div class="col-span-9 py-1 px-4 cursor-pointer " wire:click="selectNotebook({{ $notebook->id }})">
          {{ $notebook->name }}
        </div>
      </div>
    @endforeach
  @endif
</div>
