<div>
  <div class="grid grid-cols-12 w-full">
    <div class="col-span-3 xl:col-span-2 flex self-center h-8 w-8 cursor-pointer">
      <a href={{ url('links/create') }}>
        <x-heroicon-o-plus />
      </a>
    </div>
    <div class="col-span-9 xl:col-span-10 font-nunito_bold text-xl text-blue-500 px-4">
      BOOKMARKS
    </div>
  </div>

  @include('livewire.link.index')

</div>
