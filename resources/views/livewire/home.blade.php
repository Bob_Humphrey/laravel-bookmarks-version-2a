<div>

  @include('livewire.forms.flash')

  <div class="xl:grid grid-cols-12 gap-x-16">
    <div class="col-span-2">
      <livewire:notebooks />
    </div>
    <div class="col-span-3">
      <livewire:categories />
    </div>
    <div class="col-span-7">
      <livewire:links />
    </div>
  </div>
</div>
