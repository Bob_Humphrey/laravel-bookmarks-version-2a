  <x-alert type="success" class="bg-green-700 text-white p-4 mb-4 rounded" />
  <x-alert type="warning" class="bg-yellow-700 text-white p-4 mb-4 rounded" />
  <x-alert type="danger" class="bg-red-700 text-white p-4 mb-4 rounded" />
