    {{-- NAME --}}

    <div class="grid grid-cols-12">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="name" />
      </div>
      <div class="col-span-9">
        @if ($disabled)
          <x-input name="name" wire:model="name" class="p-2 rounded border border-gray-200 w-full appearance-none"
            disabled />
        @else
          <x-input name="name" wire:model="name" class="p-2 rounded border border-gray-200 w-full appearance-none" />
        @endif
      </div>
    </div>

    <div class="grid grid-cols-12 mb-4">
      <div class="col-span-3">
      </div>
      <div class="col-span-9">
        <x-error field="name" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>
