    {{-- CATEGORY --}}

    <div class="grid grid-cols-12">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="category" />
      </div>
      <div class="col-span-9">
        <select name="category" id="category" wire:model="category"
          class="p-2 rounded border border-gray-200 w-full appearance-none" {{ $disabled }}>
          <option value="0">Pick a category...</option>
          @foreach ($categories as $key => $value)
            <option value={{ $value['id'] }}>{{ $value['name'] }}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="grid grid-cols-12 mb-4">
      <div class="col-span-3">
      </div>
      <div class="col-span-9">
        <x-error field="category" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>

    {{-- TITLE --}}

    <div class="grid grid-cols-12">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="title" />
      </div>
      <div class="col-span-9">
        @if ($disabled)
          <x-input name="title" wire:model="title" class="p-2 rounded border border-gray-200 w-full appearance-none"
            disabled />
        @else
          <x-input name="title" wire:model="title" class="p-2 rounded border border-gray-200 w-full appearance-none" />
        @endif
      </div>
    </div>

    <div class="grid grid-cols-12 mb-4">
      <div class="col-span-3">
      </div>
      <div class="col-span-9">
        <x-error field="title" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>

    {{-- URL --}}

    <div class="grid grid-cols-12">
      <div class="col-span-3 flex flex-col self-center text-right pr-4">
        <x-label for="url" />
      </div>
      <div class="col-span-9">
        @if ($disabled)
          <x-input name="url" wire:model="url" class="p-2 rounded border border-gray-200 w-full appearance-none"
            disabled />
        @else
          <x-input name="url" wire:model="url" class="p-2 rounded border border-gray-200 w-full appearance-none" />
        @endif
      </div>
    </div>

    <div class="grid grid-cols-12 mb-4">
      <div class="col-span-3">
      </div>
      <div class="col-span-9">
        <x-error field="url" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>

    <div class="grid grid-cols-12 mb-8">
      <div class='col-start-4 col-span-4'>
        <input type="checkbox" name="reading_list" wire:model="readingList">
        <x-label for="reading_list" />
      </div>
      <div class='col-span-4'>
        <input type="checkbox" name="learning_list" wire:model="learningList">
        <x-label for="learning_list" />
      </div>
    </div>
