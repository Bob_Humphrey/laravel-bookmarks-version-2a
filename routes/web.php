<?php

use App\Http\Livewire\Home;
use App\Http\Livewire\ReadingList;
use App\Http\Livewire\LearningList;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Home::class);
Route::get('/home', Home::class)->name('home');
Route::get('/reading-list', ReadingList::class);
Route::get('/learning-list', LearningList::class);
Route::get('/notebooks/create', App\Http\Livewire\Notebook\Add::class)->middleware('auth');
Route::get('/notebooks/{id}/edit', App\Http\Livewire\Notebook\Update::class)->middleware('auth');
Route::get('/notebooks/{id}/delete', App\Http\Livewire\Notebook\Delete::class)->middleware('auth');
Route::get('/categories/create', App\Http\Livewire\Category\Add::class)->middleware('auth');
Route::get('/categories/{id}/edit', App\Http\Livewire\Category\Update::class)->middleware('auth');
Route::get('/categories/{id}/delete', App\Http\Livewire\Category\Delete::class)->middleware('auth');
Route::get('/links/create', App\Http\Livewire\Link\Add::class)->middleware('auth');
Route::get('/links/{id}/edit', App\Http\Livewire\Link\Update::class)->middleware('auth');
Route::get('/links/{id}/delete', App\Http\Livewire\Link\Delete::class)->middleware('auth');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
  return view('dashboard');
})->name('dashboard');

Route::get('logout', function () {
  Auth::logout();
  return redirect()->route('home');
});
